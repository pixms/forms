<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Forms;

/**
 * Description of Form
 *
 * @author Sonia
 */
class Form
{
    protected $formData;
    protected $errors;
    protected $sections = [];
    protected $action;
    protected $method;
    protected $classes;
    protected $attributes;
    
    /**
     * 
        action: "",
        method: "post",
        attributes: {enctype: "multipart/form-data"},
        classes: [],
     */
    public function __construct($action, $method, $data, $errors = [], $classes = [], $attributes = [])
    {
        $this->formData = $data;
        $this->action = $action;
        $this->method = $method;
        $this->errors = $errors;
        $this->classes = $classes;
        $this->attributes = $attributes;
        // $form->addSection()->addRow()->addRow()->addSection()->end()->end()
    }
    
    /**
     * 
                label: "Personal Information",
                classes: ["class1", "class2"],
                attributes: {
                    "placeholder": "My Placeholder",
                    "readonly": true, //Produces readonly="readonly"
                    "data-sort": "true", //Produces data-sort="true"
                },
                items: [
     */
    public function addSection($label, $classes = [], $attributes = []) {
        $section = new Section($this, $label, $classes, $attributes);
        $this->sections[] = $section;
        return $section;
    }
    
    public function addFormData($key, $data) {
        $keys = $this->convertToKeys($key);
        $array = &$this->formData;
        foreach ($keys as $key) {
            $array = &$array[$key];
        }
        $array = $data;
        return $this;
    }
    
    public function getErrors($field) {
        //Support for arrays foo[bar][baz]
        $keys = $this->convertToKeys($field);
        $errors = $this->errors;
        foreach ($keys as $key) {
            if (isset($errors[$key])) {
                $errors = $errors[$key];
            } else {
                return [];
            }
        }
        
        return $errors;
    }
    
    public function getValue($field) {
        //Support for arrays foo[bar][baz]
        $keys = $this->convertToKeys($field);
        $data = $this->formData;
        foreach ($keys as $key) {
            if (isset($data[$key])) {
                $data = $data[$key];
            } else {
                return '';
            }
        }
        
        return $data;
    }
    
    public function getSections()
    {
        return $this->sections;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getClasses()
    {
        return $this->classes;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }
    
    protected function convertToKeys($field) {
     $matches = [];
     preg_match_all('#\[?([^\[\]]*)\]?#', $field, $matches);
     //TODO See why the last item is empty
     array_pop($matches[1]);
     
     return $matches[1];
    }


}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Forms;

/**
 * Description of Item
 *
 * @author Sonia
 */
class Item
{
    protected $type;
    protected $name;
    protected $label;
    protected $config;
    
    public function __construct($type, $label, $name = null, $config = array())
    {
        $this->type = $type;
        $this->name = $name;
        $this->label = $label;
        $this->config = $config;
    }
    
    public function getType()
    {
        return $this->type;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLabel()
    {
        return $this->label;
    }

        
    public function __get($property) {
        if (isset($this->config[$property])) {
            return $this->config[$property];
        }
        
        throw new \InvalidArgumentException('The property "'.$property.'" does not exists.');
    }
    
    public function __isset($property)
    {
        if (isset($this->config[$property])) {
            return true;
        }

        return false;
    }
}

<?php

namespace Pixms\Forms;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Section
 *
 * @author Sonia
 */
class Section
{

    protected $form;
    protected $items;
    protected $label;
    protected $classes;
    protected $attributes;
    
    
    public function __construct($form, $label, $classes = [], $attributes = [], $items = [])
    {
        $this->form = $form;
        $this->items = $items;
        $this->label = $label;
        $this->classes = $classes;
        $this->attributes = $attributes;
    }

    /**
     * 
                            'label' => 'E-mail',
                            'name' => 'email',
                            'errors' => isset($errors['email']) ? $errors['email'] : array(),
                            'value' => $user->email,
                            'type' => 'text'
     */
    public function addItem($type, $label, $name = null, $config = array())
    {
        if (!is_null($name)) {
            $config['errors'] = $this->form->getErrors($name);
            $config['value'] = $this->form->getValue($name);
        }
        
        $this->items[] = new Item($type, $label, $name, $config);
        
        return $this;
    }
    
    public function end() {
        return $this->form;
    }
    
    public function getForm()
    {
        return $this->form;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getClasses()
    {
        return $this->classes;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }



}

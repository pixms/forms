<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Forms;

/**
 *
 * @author Sonia
 */
interface FormFactoryInterface
{
    /**
     * 
     * @param type $action Url to send form to
     * @param type $data Traversable
     * @param type $errors
     */
    public function create($action, array $data, array $errors = []);
}

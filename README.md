# Pixms/Forms component

This library is designed to take away the pain from building forms. The library generates an array of data that can be parsed by your favorite template renderer to build a form.

## Dependencies

Absolutely none.

## How to use

```

<?php

use Pixms\Forms\Form;
use Pixms\Forms\FormFactoryInterface;

class MyFormFactory implements FormFactoryInterface
{

    public function create($action, array $data, array $errors = []) {
        $form = new Form($action, 'post', $data, $errors, array(), array('enctype' => 'multipart/form-data'));

        $form->addSection('', ['someclassname'])
                ->addItem('radio', 'Status', 'status', ['choices' => [1 => 'Active', 0 => 'Inactive'], 'default' => 1])
                ->addItem('text', 'Name', 'name')
                ->addItem('text', 'E-mail', 'email')
                ->addItem('file', 'Image', 'image')
                ->end()->addSection('', ['someclassname'])
                ->addItem('password', 'Password', 'password')->end()
                ->addSection('', ['buttons'])
                ->addItem('link', 'Cancel', null, ['url' => '/users', 'input_attributes' => ['class' => 'button']])
                ->addItem('submit', 'Submit', null, [ 'input_attributes' => ['class' => 'ok']])->end();

        return $form;
    }

}

//In your controller

$factory = new MyFormFactory();
$form = $factory->create('', $_POST, []);

```

## Data generated

```

    form: {
        action: "",
        method: "post",
        attributes: {enctype: "multipart/form-data"},
        classes: [],
        active_classes: ["active"],
        sections: [
            {
                label: "Personal Information",
                classes: ["class1", "class2"],
                attributes: {
                    "placeholder": "My Placeholder",
                    "readonly": true, //Produces readonly="readonly"
                    "data-sort": "true", //Produces data-sort="true"
                },
                items: [
                    {

                    },
                ]
            }, (...)
        ]
    }

```

## Todos

* Provide a PHP file that actually generate the HTML
* Provide a Mustache that actually generate the HTML